**Apithy - Surveys Exam**

---

## Problema

- Se necesitan aplicar encuestas a diferentes usuarios basándonos en su edad
- Actualmente se cuenta con 3 tipos diferentes de preguntas: Rangos, Checks y Radios, cada respuesta tiene un valor específico
- La mayoría de las encuestas se evalúan de la siguiente forma (NUM_PREGUNTAS / (SUMATORIA RESPUESTAS)) * 100
- Futuras encuestas podrían tener un método particular de evaluación , debemos asegurarnos que agregarlas no sea un problema.
- Las encuestas tienen una fecha de expiración de 30 días a partir de su asignación
- Actualmente contamos con 3 encuestas pero estimamos llegar a 650 diferentes a fin de año
- Las encuestas regularmente son entregadas mediante JSON o XML y tenemos que programar lo necesario para ingresarlas en el sistema
- Necesitamos registrar en la BD los accesos de los usuarios a cada unos de las encuestas así como intentos de accesos no válidos a las encuestas y login
- [Detalle de las encuestas](https://docs.google.com/spreadsheets/d/1Ue2PCseKwpWLf-wQP7QFA75F7HIBiPBpsnr0zg7EHKo/edit?usp=sharing)


## Requerimientos

- Diseñar una BD de forma manual, y dejar el script de creación para su evaluación
- Crear un usuario específico en la base de datos con privilegios únicamente en la base de datos creada, dejar el script como evidencia
- Diseñar los data sources para las encuestas vía JSON y XML
- Generar un comando en CLI para cargar las encuestas desde un JSON,XML o un array.
- Solo usuarios autenticados pueden acceder a las encuestas y contestarlas
- Necesitamos un Servicio REST con Endpoints para crear y loguear un usuario, Obtener encuestas, Evaluar las encuestas y Consultar los resultados de las encuestas
- El método del logueo queda a tu elección
- Utilizar Symfony o Laravel para el desarrollo.
- No es obligatorio pero si deseas puedes agregar el frontend usando VueJs,Angular o React, tu creatividad será tomada en cuenta.



##Notas
- Si no logras finalizar el ejercicio no te preocupes de todas formas queremos revisar tus avances
---